import QtQuick 2.9
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import "Components"

Page {
    header: PageTop { pageTitleId: 3}

    function fillHelpText(messageNo) {
        switch(messageNo) {
        case 1:
            helpLabel.text =  i18n.tr("Changes the orientation of the level." + 
            "\nThe symbol shows the current orientation, with 0° on vertical axis." + 
            "\nWhen taking a reading, you should try to get confidence meter as near central as possible.")
            break;
        case 2:
            helpLabel.text = i18n.tr("Changes the orientation of the level." + 
            "\nThe symbol shows the current orientation, with 0° on the horizontal axis.")
            break;
        case 3:
            helpLabel.text = i18n.tr("Switches to RELATIVE from ABSOLUTE mode." + 
            "\nIn ABSOLUTE mode (the default), angles are based on appropriate axis." +  
            "\nTo set the reference for RELATIVE, tap the screen to Hold the measurement, then click the button with the rotated face." + 
            "\nYou can only select RELATIVE once you have a reference.")
            break;
        case 4:
            helpLabel.text = i18n.tr("Switches back to ABSOLUTE from RELATIVE." + 
            "\nRELATIVE is indicated by the axes aligning with the reference.")
            break;
        case 5:
            helpLabel.text = i18n.tr("Saves the current HOLD value for use in RELATIVE mode." + 
            "\nA LongPress will reset to 0 if required.")
            break;
        case 6:
            helpLabel.text = i18n.tr("Switches to display in Degrees.\nThe symbol shows the current mode.")
            break;
        case 7:
            helpLabel.text = i18n.tr("Switches to RATIO display for inclines.\nThe symbol shows the current mode.")
            break;
        case 8:
            helpLabel.text = i18n.tr("Enables the camera." + 
            "\nMake sure you are square on to the object before taking a reading." + 
            "\nTry to avoid barrel distortion. Only reference to the center of the screen." + 
            "\nWhen taking a reading, you should try to get confidence meter as near central as possible.")
            break;
        case 9:
            helpLabel.text = i18n.tr("Opens a note taking page." + 
            "\nThe last held value is used for the new note.\nYou must press enter to add the reading to the list.")
            break;
        case 10:
            helpLabel.text = i18n.tr("Opens the SETTINGS page \n" + 
            "\nThe settings only affect the main level display. All other pages follow the system theme.")
            break;
        case 11:
            helpLabel.text = i18n.tr("Help -  this screen.")
            break;
        case 12:
            helpLabel.text = i18n.tr("Reveals information about the app.")
            break;

        default:
            helpLabel.text = i18n.tr("Oops! Help not yet defined.")
        }
    }

    Item {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        ColumnLayout {
            id: contentColumn
            anchors {
                left: parent.left;
                top: parent.top;
                right: parent.right;
                margins: units.gu(2)
            }
            spacing: units.gu(2)

            Label {
                Layout.fillWidth: true
                text: i18n.tr('Click a button to find its function')
                textSize: Label.Large
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Row {
                spacing: units.gu(1)

                Icon {
                    width: units.gu(10); height: units.gu(10)
                    source: "../img/vLevel.svg"
                    //color: _textColor
                    MouseArea {
                        anchors.fill: parent
                        onClicked: fillHelpText(1)
                    }
                }
                Icon {
                    width: units.gu(10); height: units.gu(10)
                    source: "../img/hLevel.svg"
                    //color: _textColor
                    MouseArea {
                        anchors.fill: parent
                        onClicked: fillHelpText(2)
                    }
                }
                Icon {
                    width: units.gu(10); height: units.gu(10)
                    source: "../img/abs.svg"
                    //color: _textColor
                    MouseArea {
                        anchors.fill: parent
                        onClicked: fillHelpText(3)
                    }
                }
                Icon {
                    width: units.gu(10); height: units.gu(10)
                    source: "../img/rel.svg"
                    //color: _textColor
                    MouseArea {
                        anchors.fill: parent
                        onClicked: fillHelpText(4)
                    }
                }
            }

            Row {
                Icon {
                    width: units.gu(10); height: units.gu(10)
                    name: "view-rotate"
                    //color: _textColor
                    MouseArea {
                        anchors.fill: parent
                        onClicked: fillHelpText(5)
                    }
                }
                Icon {
                    width: units.gu(10); height: units.gu(10)
                    source: "../img/deg.svg"
                    //color: _textColor
                    MouseArea {
                        anchors.fill: parent
                        onClicked: fillHelpText(7)
                    }
                }
                Icon {
                    width: units.gu(10); height: units.gu(10)
                    source: "../img/inc.svg"
                    //color: _textColor
                    MouseArea {
                        anchors.fill: parent
                        onClicked: fillHelpText(6)
                    }
                }
                Icon {
                    width: units.gu(10); height: units.gu(10)
                    name: "camera-app-symbolic"
                    //color: _textColor
                    MouseArea {
                        anchors.fill: parent
                        onClicked: fillHelpText(8)
                    }
                }
            }

            Row {
                Label {
                    width: units.gu(10); height: units.gu(10)
                    text: "✍"
                    //color: _textColor
                    font.pixelSize: units.gu(8)
                    horizontalAlignment: Text.AlignHCenter
                    MouseArea {
                        anchors.fill: parent
                        onClicked: fillHelpText(9)
                    }
                }
                Label {
                    width: units.gu(10); height: units.gu(10)
                    text: "⚙"
                    //color: _textColor
                    font.pixelSize: units.gu(7)
                    horizontalAlignment: Text.AlignHCenter
                    MouseArea {
                        anchors.fill: parent
                        onClicked: fillHelpText(10)
                    }
                }
                Label {
                    width: units.gu(10); height: units.gu(10)
                    text: "❔"
                    //color: _textColor
                    font.pixelSize: units.gu(7)
                    horizontalAlignment: Text.AlignHCenter
                    MouseArea {
                        anchors.fill: parent
                        onClicked: fillHelpText(11)
                    }
                }
                Label {
                    width: units.gu(10); height: units.gu(10)
                    text: "🛈"
                    //color: _textColor
                    font.pixelSize: units.gu(8)
                    horizontalAlignment: Text.AlignHCenter
                    MouseArea {
                        anchors.fill: parent
                        onClicked: fillHelpText(12)
                    }
                }
            }

            Label {
                id: helpLabel
                Layout.fillWidth: true
                text: i18n.tr('Click one of the above buttons for help');
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
            }
        }
    }
}