import QtQuick 2.9
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import "Components"

Page {
    header: PageTop { pageTitleId: 5}
    property color localBackground: _backgndColor
    property color localText: _textColor
    property color localGrid: _gridColor
    property color localDatum: _datumColor
    property int localTHeight: _textHeight
    property int localDWidth: _datumWidth
    property int localGWidth: _gridWidth
    property real localButtonWidth: root.width / 2 - units.gu(6)
    property real localButtonRadius: units.gu(1.2)
    property real localButtonBorder: units.gu(0.4)
    property int _stage : 1
    property int demoRot: 0
    property bool _save: false

    Component.onCompleted: {
        fillPrompts (1)
    }

    property string _promptProc: i18n.tr("Process")
    property string _promptNoise: i18n.tr("Noise Eval")
    property string _promptCal: i18n.tr("Calibrating")
    property string _promptCase: i18n.tr("Case Offset")

    property string _runMsg:  i18n.tr("You will be guided through rocking the device on the pivot in order to calibrate each axis. ")

    property string _now:  i18n.tr("Now, ")
    property string _again:  i18n.tr("Again, ")
    property string _lastTime:  i18n.tr("For the last time, ")


    function runAction() {
        switch(_stage) {
        case 1:
            console.log("==== restore non-Cal " + Qt.formatDateTime(new Date(), "hh:mm:ss") + "====")
            _save = true
//          cx = cy = 0 etc;
            mainStack.pop() ; mainStack.push(Qt.resolvedUrl('SettingsPage.qml'))
            break;
        case 2:
            console.log("==== skip noise " + Qt.formatDateTime(new Date(), "hh:mm:ss") + "====")
            _stage = 6
            fillPrompts()
            break;
        case 3:
        case 4:
        case 5:
            console.log("==== cancel CAl " + Qt.formatDateTime(new Date(), "hh:mm:ss") + "====")
            _save = false
            mainStack.pop() ; mainStack.push(Qt.resolvedUrl('SettingsPage.qml'))
            break;
        case 6:
            console.log("==== Cal AxV- " + Qt.formatDateTime(new Date(), "hh:mm:ss") + "====")
            _stage = 14
            fillPrompts()
            break;
        case 7:
            console.log("==== Cal AxH- " + Qt.formatDateTime(new Date(), "hh:mm:ss") + "====")
            break;
        case 8:
            console.log("==== next Prompt " + Qt.formatDateTime(new Date(), "hh:mm:ss") + "====")
            break;
        case 9:
            console.log("==== next Prompt " + Qt.formatDateTime(new Date(), "hh:mm:ss") + "====")
            break;
        case 10:
            console.log("==== Cal AxV+ " + Qt.formatDateTime(new Date(), "hh:mm:ss") + "====")
            break;
        case 14:
        case 15:
        case 16:
            console.log("==== restart Cal " + Qt.formatDateTime(new Date(), "hh:mm:ss") + "====")
            visualPrompt.visible = true
            _stage = 1
            fillPrompts()
            break;
        case 17:
            _save = false
            mainStack.pop() ; mainStack.push(Qt.resolvedUrl('SettingsPage.qml'))
        default:
            console.log("==== OOPS! " + Qt.formatDateTime(new Date(), "hh:mm:ss") + "====")
            break;
        }
    }

    function fillPrompts() {
        switch(_stage) {
        case 1:
            prompt1.text =  _promptProc  
            prompt2.text =  i18n.tr("Calibration is done in 3 stages.\n" + 
            "First, the app needs to gather sensor noise readings.\n" + 
            "Secondly, the actual calibration process.\n"+ 
            "Finally, the app needs to calculate any offset created by your case.\n\n" + 
            "This process is best done against a window pane. You may find it easier " +
            "if you place a tissue or small cloth behind the device to allow it to slide smoothly on the pane.\n" +
            "You will also need something to act as a pivot point such as a bottle cap or small triangular prism.\n\n" + 
            "We will start with noise estimation. ") + 
            i18n.tr("\nTap anywhere to continue.")

            visualPrompt.visible = false
            vFrame.visible =  false
            multiButton.color = theme.palette.normal.negative
            multiButton.text = i18n.tr("Restore Factory Preset")
            _indicator.color = UbuntuColors.ash
            break;
        case 2:
            prompt1.text = _promptNoise
            prompt2.text =  i18n.tr("Stand the device on the window frame with its back the against the pane in") + 
            i18n.tr(" PORTRAIT mode. ") + i18n.tr("\nTap anywhere to continue.")

            visualPrompt.visible = true
            demoRot = 0
            vFrame.visible =  false
            pivot.visible = false
            calibLevel.rotation = 0

            multiButton.color = UbuntuColors.orange
            multiButton.text = i18n.tr("Skip")
            _indicator.color = UbuntuColors.ash
            break;

        case 3:
            prompt1.text = _promptNoise
            prompt2.text =  i18n.tr("\n\nWait for the indicator to turn green.") + i18n.tr("\nTap anywhere to continue.")

            calibLevel.rotation = 0
            demoRot = 0
            pivot.visible =false
            multiButton.color = theme.palette.normal.negative
            multiButton.text = i18n.tr("Cancel")
            _indicator.color = UbuntuColors.green
            break;

        case 4:
            prompt1.text = _promptProc
            prompt2.text =  i18n.tr("Re-orient the device as shown in ") + 
            i18n.tr("LANDSCAPE mode. ") +
            i18n.tr("\nTap anywhere to continue.")

            calibLevel.rotation = 90
            demoRot = 0
            pivot.visible =false
            _indicator.color = UbuntuColors.ash
            break;

        case 5:
            prompt1.text = _promptNoise
            prompt2.text =  i18n.tr("\n\nWait for the indicator to turn green.") + 
            i18n.tr("\nTap anywhere to continue.")

            visualPrompt.visible = true
            calibLevel.rotation = 90
            demoRot = 0
            pivot.visible =false
            multiButton.text = i18n.tr("Cancel")
            _indicator.color = UbuntuColors.green
            break;

        case 6:
            prompt1.text = _promptProc 
            prompt2.text =  i18n.tr( "This is the start of the Actual calibration." + 
            "You will need to test the positive and negative accelerometers in both X and Y\n\n" + 
            "Place the pivot on the window frame and hold the device on top in") + 
            i18n.tr(" PORTRAIT mode. ") + 
            i18n.tr("When prompted, you should roll the device from side to side on the pivot." +
            "Eventually the indicator will turn green. You can then rotate to the next orientation.") + 
            i18n.tr("\nTap anywhere to continue.")

            visualPrompt.visible = true
            demoRot = -3
            calibLevel.rotation = 0
            pivot.visible =true
            multiButton.color = UbuntuColors.orange
            multiButton.text = i18n.tr("Skip")
            _indicator.color = UbuntuColors.ash
            break;

        case 7:
            prompt1.text = _promptCal
            prompt2.text =  _now + i18n.tr("Slowly roll the device from side to side on the pivot. Keep your eye on the arrows. " +
            "Do not let them go red.") + 
            i18n.tr("\n\nWait for the indicator to turn green.") + 
            i18n.tr("\nTap anywhere to continue.")

            calibLevel.rotation = 0
            multiButton.color = theme.palette.normal.negative
            multiButton.text = i18n.tr("Cancel")
            _indicator.color = UbuntuColors.green
            break;

        case 8:
            prompt1.text = _promptProc
            prompt2.text =  _now + i18n.tr(" rotate the device a quarter turn anti-clockwise.") + "\n\n" + 
            i18n.tr("Place the pivot on the window frame and hold the device on top in ") + i18n.tr("LANDSCAPE mode. ") + 
            i18n.tr("\nTap anywhere to continue.")

            calibLevel.rotation = 90
            demoRot = -3
            _indicator.color = UbuntuColors.ash
            break;

        case 9:
            prompt1.text = _promptCal
            prompt2.text =  _now + i18n.tr("Slowly roll the device from side to side on the pivot. Keep your eye on the arrows. " +
            "Do not let them go red.") + 
            i18n.tr("\n\nWait for the indicator to turn green.") + 
            i18n.tr("\nTap anywhere to continue.")

            _indicator.color = UbuntuColors.green
            break;

        case 10:
            rotation = 180
            prompt1.text = _promptProc
            prompt2.text = _again + i18n.tr(" rotate the device a quarter turn anti-clockwise.") + "\n\n" + 
            i18n.tr("Place the pivot on the window frame and hold the device on top in ") + i18n.tr("INVERTED") + 
            i18n.tr(" PORTRAIT mode. ") + i18n.tr("\nTap anywhere to continue.")

            calibLevel.rotation = 180
            demoRot = -3
            _indicator.color = UbuntuColors.ash
            break;

        case 11:
            prompt1.text = _promptCal
            prompt2.text =  _now + i18n.tr("Slowly roll the device from side to side on the pivot. Keep your eye on the arrows. " +
            "Do not let them go red.") + "\n\n" + 
            i18n.tr("\n\nWait for the indicator to turn green.") + i18n.tr("\nTap anywhere to continue.")

            _indicator.color = UbuntuColors.green
            break;

        case 12:
            rotation = 0
            prompt1.text = _promptProc
            prompt2.text =  _again + i18n.tr(" rotate the device a quarter turn anti-clockwise.") + "\n\n" + 
            i18n.tr("Place the pivot on the window frame and hold the device on top in ") + "INVERTED " + 
            i18n.tr("LANDSCAPE mode. ") + i18n.tr("\nTap anywhere to continue.")

            calibLevel.rotation = 270
            _indicator.color = UbuntuColors.ash
            demoRot = -3
            break;

        case 13:
            prompt1.text = _promptCal
            prompt2.text =  _lastTime + i18n.tr("Slowly roll the device from side to side on the pivot. Keep your eye on the arrows. " +
            "Do not let them go red.") + "\n" + 
            i18n.tr("\n\nWait for the indicator to turn green.") + i18n.tr("\nTap anywhere to continue.")

            _indicator.color = UbuntuColors.green
            break;

        case 14:
            prompt1.text = _promptCase
            prompt2.text =  _now + i18n.tr("we need to make a record of the offset caused by the case.\n" + 
            "It will be easiest to do this in a door opening where you can read from both sides.\n" +  
            "You will be asked to flip the device, after the first reading, so that the screen faces the other way." + 
            "For now, take a normal first reading in ") + i18n.tr("PORTRAIT mode. ") + 
            i18n.tr("\nTap anywhere to continue.")

            vFrame.visible =  true
            pivot.visible = false
            calibLevel.rotation = 0
            multiButton.text = i18n.tr("Return to start")
            _indicator.color = UbuntuColors.ash
            break;

        case 15:
            prompt1.text = _promptCase
            prompt2.text =  _now + i18n.tr("Now for the difficult bit. " +
            "You need to turn the device upside down with the same edge in contact the vertical " + 
            "but with the screen facing the opposite way.") + "\n\n" + 
            i18n.tr("\nTap anywhere to continue.")
            rotation = 180
            break;

        case 16:
            //vFrame.visible = false
            visualPrompt.visible = false
            prompt1.text = _promptProc
            prompt2.text = i18n.tr("Thank you. Calibration of your device is now complete.\n") +
            i18n.tr("\nTap anywhere to continue.")     
            rotation = 0
            break;

        default:
            mainStack.pop()
            mainStack.push(Qt.resolvedUrl('SettingsPage.qml'))
            break;
        }
    }

    Item {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        ColumnLayout {
            id: contentColumn
            anchors {
                left: parent.left;
                top: parent.top;
                right: parent.right;
                margins: units.gu(2)
            }
            spacing: units.gu(2)
            RowLayout {
                Label {
                    id: prompt1
                    anchors.left: parent.left
                    Layout.fillWidth: true
                    textSize: Label.Large
                    wrapMode: Text.WordWrap
                }
                Button {
                    id: multiButton
                    anchors.right: parent.right
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                    width: localButtonWidth * 1.5
                    text: i18n.tr("Restore Factory Preset")
                    color: theme.palette.normal.negative
                    onClicked: {runAction()}
                }
            }
            Label {
                id: prompt2
                Layout.fillWidth: true
//                text: 'Tap anywhere to start'
                wrapMode: Text.WordWrap
            }
        }
        Rectangle {
            id: visualPrompt
            visible: false
            width: parent.width ; height: parent.height / 2 // * 1.2
            color: 'transparent' ; border.color: UbuntuColors.ash
            anchors {
                bottom: parent.bottom
                horizontalCenter: parent.horizontalCenter
            }
            Rectangle {
                color: 'transparent'
                width: parent.width / 8 ; height: width
                radius: localButtonRadius ;  border.width: localButtonBorder ; border.color: _indicator.color
                anchors {
                    top: parent.top
                    horizontalCenter: parent.horizontalCenter
                }
                Icon {
                    id: _indicator
                    width: parent.width *.75 ; height: parent.width
                    anchors.centerIn: parent
                    name: "ok"
                    color: theme.palette.normal.backgroundTertiaryText
                }     
            }
            Rectangle {
                id: demoBox
                color: 'transparent'
                width: parent.width / 2 ; height: parent.width / 2
                rotation: demoRot
                anchors {
                    bottom: (pivot.visible) ? pivot.top : pivot.bottom //verticalCenter
                    horizontalCenter: parent.horizontalCenter
                }
                Icon {
                    id: calibLevel
                    width: parent.width ; height: parent.width
                    anchors {
                        centerIn: parent
                        verticalCenterOffset : (rotation == 0 || rotation == 180 ) ?  0  : height / 4
                    }
                    source: "../img/vLevel.svg"
                    color: theme.palette.normal.backgroundTertiaryText
                }
            }
            Rectangle {
                id: pivot
                color: UbuntuColors.blue
                width: parent.width / 11 ; height: parent.width / 11 ; rotation: 45
                anchors {
                    bottom: parent.bottom
                    horizontalCenter: parent.horizontalCenter
                }
            }
/*            Rectangle {
                id: pivot
                color: UbuntuColors.red
                width: parent.width / 11 ; height: parent.width / 11 ; radius: width / 2
                anchors {
                    bottom: parent.bottom
                    horizontalCenter: parent.horizontalCenter
                }
            }*/
            Rectangle {
                id: vFrame
                property real _offset:  calibLevel.width / -2
                visible: false
                color: theme.palette.normal.backgroundTertiaryText
                width: parent.width / 5 ; height: visualPrompt.height
                anchors {
                    top: visualPrompt.top
                    horizontalCenter: parent.horizontalCenter
                    horizontalCenterOffset: _offset
                }
            }
            Icon {
                id: vPush
                width: vFrame.width ; height: vFrame.width
                anchors {
                    centerIn: parent
                    verticalCenterOffset : calibLevel.height / 3
                    horizontalCenterOffset: - vFrame._offset
                }
                visible: vFrame.visible
                source: "../img/arrow.svg"
                rotation: (vFrame._offset>0) ? 90 : -90
                color: vFrame.color
            }
        }
        AbstractButton {
            anchors {
                top: parent.top
                topMargin: units.gu(20)
                bottom: parent.bottom
                //bottomMargin: units.gu(8)
            }
            width: parent.width
            onClicked: fillPrompts(++_stage)
        }
    }
}
