import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import "Components"

Item {
    id: digiLevel
    anchors.fill: parent
    anchors.topMargin: 0
    anchors.bottomMargin: 0

    function formattedReadout( _decimals) {
        if ( _decimals == null) { _decimals = 100}
        if (!inclvl) {
            if (abslvl){
                return (Math.abs(Math.round(theta * _decimals)) % (360 * _decimals))/_decimals + "°" ;
            } else {
                return (Math.abs(Math.round((theta - relTheta) * _decimals)) % (360 * _decimals))/_decimals + "°" ;
            }
        } else { 
            var tanThetaRad = grdnt
            if (tanThetaRad <0.01) {
                return "1:" + Math.round(1 / tanThetaRad);
            } else if (tanThetaRad < 1) {
                return "1:" + Math.round(10 / tanThetaRad)/10;
            } else if (tanThetaRad < 1) {
                return "1:" + Math.round(100 / tanThetaRad)/100;
            } else if (tanThetaRad < 100) {
                return  Math.round(10 * tanThetaRad)/10 + ":1" ;
            } else {
                return  Math.round(tanThetaRad) + ":1" ;
            }
        }
    }

    function gridRotation(){
        if (!abslvl) {
            if  (levelOrientation == 0) {
                return -relTheta
            } else {
                return relTheta
            }
        } else { return 0 }
    }

    function zoomBubble(direction) {
        if(direction) {
            _scale = _scale *2
            if (_scale > 8) {_scale = 8}
        } else {
            _scale = _scale / 2
            if (_scale < 2) {_scale = 1}
        }
        
    }

    Rectangle {
        id: levelRoot
        height: root.height ; width: root.width
        anchors.topMargin: units.gu(8)
        color: _backgndColor

        Rectangle {
             id: _controls //spacer only, actual controls defined later
            height: units.gu(6) ; width: levelRoot.width
            color: 'transparent'
        }

        CameraLevel {
            visible: camLevel && !radialBubble
        }

        Dial {
            id: _dialZ
            visible: !radialBubble
            implicitWidth: parent.width / 3
            rotation: (levelOrientation == 0)? 90 : 0
            anchors {
                top: _controls.bottom
                margins: units.gu(2)
                left: _controls.left
                horizontalCenterOffset: units.gu(10)
            }
            _borderCol: _gridColor
            _dialText: i18n.tr("\nconfidence")
            _textSize: Label.Medium
            _rotation: (Math.abs(az) < 1.5) ? az * 60 : 180
        }

        Rectangle {
            id: vbaseline
            height: parent.height ; width: _gridWidth
            anchors.horizontalCenter: parent.horizontalCenter
            color: _gridColor
            rotation: gridRotation()
        }

        Rectangle {
            id: hbaseline
            height: _gridWidth ; width: parent.width
            anchors.verticalCenter: parent.verticalCenter
            color: _gridColor
            rotation: gridRotation()
        }

        BubbleLevel {
            id: bubbleLevel
            visible: radialBubble
        }

        StandardLevel {
            id: vertLevel
            visible: !radialBubble && !camLevel
            rotation: (levelOrientation == 0)? -theta : theta
            _lineWidth: _datumWidth
        }

        AbstractButton {
            anchors {
                top: _controls.bottom
                topMargin: units.gu(1)
                bottom: levelRoot.bottom
                bottomMargin: units.gu(8)
            }
            width: parent.width
            onClicked: held = !held
        }

        Label {
            id: promptText
            anchors { 
                bottom: parent.bottom
                bottomMargin: units.gu(1.5)
                horizontalCenter: parent.horizontalCenter 
            }
            text: (!held) ? i18n.tr("tap anywhere to hold") : i18n.tr("tap to release")
            textSize: Label.Large
            color: _textColor
        }
    }
    Row {
       anchors {
            top: parent.top
            topMargin: units.gu(1)
        }
        spacing: (levelRoot.width - units.gu(25)) / 7
        Rectangle { width: units.gu(0.1); height: units.gu(4) ; color: "transparent" }
        Rectangle { 
            width: units.gu(4); height: units.gu(4) ; color: "transparent" 
            Icon {
                width: units.gu(4); height: units.gu(4)
                visible: !radialBubble
                source: (levelOrientation) ? "../img/vLevel.svg": "../img/hLevel.svg"
                color: _textColor
                MouseArea {
                    anchors.fill: parent
                    onClicked: (levelOrientation ==0) ? levelOrientation = 90 : levelOrientation = 0
                }
            }
            Icon {
                width: units.gu(4); height: units.gu(4)
                name: "zoom-in"
                visible: radialBubble
                color: _textColor
                MouseArea {
                    anchors.fill: parent
                    onClicked:  zoomBubble(true)
                }
            }
        }
        Rectangle { 
            width: units.gu(4); height: units.gu(4) ; color: "transparent" 
            Icon {
                width: units.gu(4); height: units.gu(4)
                visible: !radialBubble
                source: (abslvl) ? "../img/abs.svg": "../img/rel.svg"
                color: _textColor
                MouseArea {
                    anchors.fill: parent
                    onClicked: if (relTheta != 0) {abslvl = !abslvl} else {abslvl= true}
                    onPressAndHold: relTheta = 0
                }
            }
        }
        Rectangle { 
            width: units.gu(4); height: units.gu(4) ; color: "transparent" 
            Icon {
                width: units.gu(4); height: units.gu(4)
                visible: !radialBubble
                name: "view-rotate"
                color: (relTheta == 0) ? _textColor : _gridColor
                MouseArea {
                    anchors.fill: parent
                    onClicked: relTheta = theta
                    onPressAndHold: relTheta = 0
                }
            }
            Icon {
                width: units.gu(4); height: units.gu(4)
                name: "zoom-out"
                color: _textColor
                visible: radialBubble
                MouseArea {
                    anchors.fill: parent
                    onClicked:  zoomBubble(false)
                }
            }
        }
        Rectangle { 
            width: units.gu(4); height: units.gu(4) ; color: "transparent"
            Icon {
                width: units.gu(4); height: units.gu(4)
                visible: !radialBubble
                source: (inclvl) ? "../img/inc.svg": "../img/deg.svg"
                color: _textColor
                MouseArea {
                    anchors.fill: parent
                    onClicked: inclvl = !inclvl
                }
            }
        }
        Rectangle { 
            width: units.gu(4); height: units.gu(4) ; color: "transparent" 
            Icon {
                width: units.gu(4); height: units.gu(4)
                visible: !radialBubble
                name: "camera-app-symbolic"
                color: _textColor
                MouseArea {
                    anchors.fill: parent
                    onClicked: camLevel = !camLevel
                }
            }
        }
        Rectangle { 
            width: units.gu(4); height: units.gu(4)
            color: _textColor
            ToolButton {
                id: dropMenu
                text: qsTr(" ≡") //≣☰
                font.pointSize: 36
                font.weight: Font.DemiBold
                onClicked: _menu.open()
                Menu {
                    id: _menu
                    MenuItem {
                        text: "  ≡"
                        font.pointSize: 48
                    }
                    MenuItem {
                        text: " ✍ "
                        font.pointSize: 44
                        onTriggered: {mainStack.push(Qt.resolvedUrl('NotesPage.qml'))}
                    }
                    MenuItem {
                        text: " ⚙ "
                        font.pointSize: 44
                        onTriggered: {mainStack.push(Qt.resolvedUrl('SettingsPage.qml'))}
                    }
                    MenuItem {
                        text: " ❔ "
                        font.pointSize: 44
                        onTriggered: {mainStack.push(Qt.resolvedUrl('HelpPage.qml'))}
                    }
                    MenuItem { 
                        text: " 🛈 "
                        font.pointSize: 56
                        onTriggered: {mainStack.push(Qt.resolvedUrl('AboutPage.qml'))}
                    }
                }
            }
        }
    }
}