import QtQuick 2.9
import Ubuntu.Components 1.3

Item {
    id: dial
    implicitWidth: parent.width / 2 + units.gu(3)
    implicitHeight: width//parent.width / 2 + units.gu(3)

    property alias _borderCol: dialBackground._borderCol
    property alias _dialText: dialText.text
    property alias _textSize: dialText.textSize
    property alias _rotation: dialHand.rotation
    //property alias _size: implicitWidth

    DialBackground {
        id: dialBackground

        DialDigits { }

        DialHand {
            id: dialHand
            width: parent.border.width * 0.2
            height: parent.radius - parent.border.width * 2
            color: "red"
        }

        Rectangle {
            width: parent.width * 0.04
            height: width
            anchors.centerIn: parent
            radius: width / 2
            color: "black"
        }

        Label {
            id: dialText
            anchors.centerIn: parent
            text: " \n\n*"
            textSize: Label.Medium
            color: _textColor
            wrapMode: Text.WordWrap
        }
    }
}
