import QtQuick 2.9

Item {
    implicitWidth: parent.width / 2 + units.gu(3)
    implicitHeight: parent.width / 2 + units.gu(3)

    property alias _seconds: secondHand.rotation
    property alias _minutes: minuteHand.rotation
    property alias _hours: hourHand.rotation

    DialBackground {
        id: _face
        ClockDigits { }

        DialHand {
            id: hourHand
            width: parent.border.width * 0.8
            height: parent.radius - parent.border.width * 7
        }

        DialHand {
            id: minuteHand
            width: parent.border.width * 0.5
            height: parent.radius - parent.border.width * 2
        }

        DialHand {
            id: secondHand
            width: parent.border.width * 0.2
            height: parent.radius - parent.border.width * 2
            color: "red"
        }

        Rectangle {
            width: parent.width * 0.04
            height: width
            anchors.centerIn: parent
            radius: width / 2
            color: "black"
        }
    }
}
