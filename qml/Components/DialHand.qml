import QtQuick 2.9

Rectangle {
    antialiasing: true
    color: "black"
    transformOrigin: Item.Bottom
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.bottom: parent.verticalCenter
}
