import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtMultimedia 5.9
import Ubuntu.Components 1.3

Item {
    anchors.fill: parent
    property var cameras: QtMultimedia.availableCameras
    property int currentCamera: 0
    property real localButtonRadius: units.gu(1.2)
    property real localButtonBorder: units.gu(0.4)

    ColumnLayout {
        anchors.fill: parent
        anchors.top: parent.top
        anchors.topMargin: units.gu(5)

        VideoOutput {
            id: liveVideo
            Layout.fillWidth: true
            Layout.fillHeight: true
            source: Camera { id: camera }
            autoOrientation: false
            orientation: 270
        }

        Button {
            visible: cameras.length > 1
            anchors { 
                bottom: parent.bottom
                bottomMargin: units.gu(6)
                right: liveVideo.right
                rightMargin: units.gu(2.5)
                left: undefined
            }
            text: i18n.tr("Change camera")
            color: _gridColor

            onClicked: {
                currentCamera++
                if (currentCamera >= cameras.length){
                    currentCamera = 0
                }
                camera.deviceId = cameras[currentCamera].deviceId
            }
        }

        Label {
            id: camAngle
            anchors {
                bottom: parent.bottom
                bottomMargin: units.gu(6)
                left: liveVideo.left
                leftMargin: units.gu(4)
                right: undefined
            }

            text: formattedReadout(10)
            textFormat: Text.RichText
            horizontalAlignment: Text.AlignRight
            font.pixelSize: _textHeight * 2
            font.weight: Font.DemiBold
            color: _textColor
        }


    }
}
