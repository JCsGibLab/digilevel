import QtQuick 2.9
import Ubuntu.Components 1.3

Rectangle {
    anchors.fill: parent
    anchors.centerIn: parent
    color: "transparent"

    function distForAngle(angle) {
        return width / 2 * Math.sin(angle * Math.PI / 90)
    }

    Rectangle {             //the floating bubble
        width: units.gu(3)
        height: width
        radius: width/2
        color: _datumColor
        x: (bubbleLevel.width - width)   / 2 + ax/Math.sqrt(ax*ax+ay*ay) * distForAngle(Math.abs(phi)) * _scale
        y: (bubbleLevel.height - height) / 2 - ay/Math.sqrt(ax*ax+ay*ay) * distForAngle(Math.abs(phi)) * _scale
    }

    Repeater {
        id: scaleGrid
        //model: [40, 30, 25, 20, 15, 10, 5, 1]
        model: [40, 30, 22, 16, 12, 8, 4, 1]

        Rectangle {
            anchors.centerIn: parent
            width: 2 * bubbleLevel.distForAngle(modelData)
            height: width
            radius: width/2
            border.color: _gridColor
            border.width: units.dp(1)
            color: "transparent"

            Label {
                x: 0.85 * parent.width
                y: 0.85 * parent.height
                text: modelData / _scale + "&deg;"
                textFormat: Text.RichText
                color: _gridColor
            }
        }
    }
}
