import QtQuick 2.9
import Ubuntu.Components 1.3

Rectangle {
    property alias _lineWidth: vlevel.width
    anchors.centerIn: parent
    height: Math.sqrt((root.height*root.height) + (root.width*root.width)) + units.gu(10)
    width: height
    color: "transparent"

    Rectangle {
        id: vlevel
        height: parent.height
        width: 4
        anchors.horizontalCenter: parent.horizontalCenter
        color: _datumColor
        border.color: 'black'
        border.width: 4
    }

    Label {
        rotation: (levelOrientation == 0) ? +90 : 0
        id: vdegrees
        width: units.gu(8)
        anchors {
            bottom: parent.verticalCenter
            left: parent.horizontalCenter
            right: undefined
            bottomMargin: (levelOrientation == 0) ? units.gu(-2) : 0
            leftMargin: (levelOrientation == 0) ? units.gu(0) : units.gu(2)
        }
        text: formattedReadout()
        textFormat: Text.RichText
        font.pixelSize: _textHeight * 2
        font.weight: Font.DemiBold
        color: _textColor
    }
/*
    Label {
        rotation: (levelOrientation == 0) ? +90 : 0
        id: vphi
        width: units.gu(8)
        anchors {
            bottom: vdegrees.bottom
            right: vdegrees.left
            left: undefined
            bottomMargin: (levelOrientation == 0) ? units.gu(0) :  units.gu(1)
            rightMargin: (levelOrientation == 0) ? units.gu(1) : units.gu(1)
        }
        text: Math.round(phi) % 360 + "&deg;"
        textFormat: Text.RichText
        font.pixelSize: _textHeight
        color: _textColor
    }*/
}

