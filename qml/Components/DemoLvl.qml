import QtQuick 2.9
import QtQuick.Layouts 1.2
//import QtLocation 5.9
import Ubuntu.Components 1.3

Rectangle {
    property int lvlTextHt: localTHeight
    property alias textColor: vdegrees.color
    property alias textHt: vdegrees.font.pixelSize
    property alias gridColor: vbaseline.color
    property alias datumColor: vlevel.color
    property alias levelWidth: vlevel.width
    property alias gridWidth: vbaseline.width 
    width: parent.width
    height: parent.height
    anchors{
        right: parent.right
        left: undefined
        margins: 0
        rightMargin: units.gu(3)
    }
    border.width: 0
    color: localBackground

    Rectangle {
        id: vbaseline
        height: parent.height
        width: 2
        anchors.horizontalCenter: parent.horizontalCenter
        color: gridColor
    }

    Rectangle {
        id: hbaseline
        width: parent.width
        height: vbaseline.width 
        anchors.verticalCenter: parent.verticalCenter
        color: vbaseline.color
    }

    Rectangle {
        id: vertLevel
        rotation: 15
        anchors.centerIn: parent
        height: parent.height
        width: height
        color: "transparent"

        Rectangle {
            id: vlevel
            height: parent.height
            width: 12
            anchors.horizontalCenter: parent.horizontalCenter
            color: bubbleColor
            border.color: 'black'
            border.width: 4
        }

        Label {
            id: vdegrees
            width: units.gu(6)
            anchors {
                bottom: parent.verticalCenter
                bottomMargin: units.gu(-1)
                left: parent.horizontalCenter
                leftMargin: units.gu(0.5)
                right: undefined
            }
            text: '15.5°'
            textFormat: Text.RichText
            font.pixelSize: lvlTextHt
            font.weight: Font.DemiBold
            color: textColor
        }

/*        Label {
            id: vphi
            width: units.gu(6)
            anchors {
                bottom: vdegrees.bottom
                bottomMargin: units.gu(1)
                right: vdegrees.left
                rightMargin: units.gu(1)
                left: undefined
            }
            text: '3°'
            textFormat: Text.RichText
            horizontalAlignment: Text.AlignRight
            font.pixelSize: vdegrees.font.pixelSize / 2
            color: vdegrees.color
        }*/
    }
    Rectangle {
        width: parent.width
        height: parent.height
        radius: units.gu(2)
        anchors.centerIn: parent
        border.color: theme.palette.normal.backgroundTertiaryText//'white'
        border.width: units.gu(2)
        color: 'transparent'
    }
}
