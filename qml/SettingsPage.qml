import QtQuick 2.9
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import "Components"

Page {
    header: PageTop { pageTitleId: 2 }

    property color localBackground: _backgndColor
    property color localText: _textColor
    property color localGrid: _gridColor
    property color localDatum: _datumColor
    property int localTHeight: _textHeight
    property int localDWidth: _datumWidth
    property int localGWidth: _gridWidth

    property int selectorHeight: Math.round(root.height / 18)
    property real sliderHeight: selectorHeight / 1.25
    property real sliderWidth: root.width / 18
    property real localButtonWidth: root.width / 2 - units.gu(6)
    property real localButtonRadius: units.gu(1.2)
    property real localButtonBorder: units.gu(0.4)

    property real oldId: 0

    function fixColors(asTheme) {
        if (asTheme) {
            localBackground = theme.palette.normal.background
            localText = theme.palette.normal.baseText
            localGrid = theme.palette.normal.base
            localDatum = theme.palette.normal.backgroundText
            localTHeight = units.dp(24)
            localDWidth = 12
            localGWidth = 4
        } else {
            localBackground = _settings.backgroundColor//_backgndColor
            localText = _settings.textColor//_textColor
            localGrid = _settings.gridColor//_gridColor
            localDatum = _settings.datumColor//_datumColor
            localTHeight = _settings.textHeight//_textHeight
            localDWidth = _settings.datumWidth//_datumWidth
            localGWidth = _settings.gridWidth//_gridWidth
        }
    }
    function _getSliderVal(colorVal, sliderId) {
        return parseInt(colorVal.toString().substr(sliderId*2 + 1, 2), 16);
    }

    function _makeColorString() {
        var valR = "00"
        var valG = "00"
        var valB = "00"
        if ( !( colorSlideR == null )) {valR = ("00" + Math.ceil(colorSlideR.value).toString(16))}
        if ( !( colorSlideG == null )) {valG = ("00" + Math.ceil(colorSlideG.value).toString(16))}
        if ( !( colorSlideB == null )) {valB = ("00" + Math.ceil(colorSlideB.value).toString(16))}

        return "#"  + valR.substring(valR.length - 2)
                    + valG.substring(valG.length - 2)
                    + valB.substring(valB.length - 2);
    }

    function clrBorders() {
        chooseBkgnd.item.border.color = theme.palette.normal.backgroundTertiaryText
        chooseText.item.border.color = theme.palette.normal.backgroundTertiaryText
        chooseDatum.item.border.color = theme.palette.normal.backgroundTertiaryText
        chooseGrid.item.border.color = theme.palette.normal.backgroundTertiaryText
    }

    function setChoice( chosenId ) {
        clrBorders()
        if ( widthSlide != null ) {
            if ( chosenId != oldId ) {
                switch ( chosenId ) {
                    case 1: {
                        colorSlideR.value = _getSliderVal(localBackground, 0)
                        colorSlideG.value = _getSliderVal(localBackground, 1)
                        colorSlideB.value = _getSliderVal(localBackground, 2)
                        chooseBkgnd.item.border.color = theme.palette.normal.selection
                        break;
                    }
                    case 2: {
                        colorSlideR.value = _getSliderVal(localText, 0)
                        colorSlideG.value = _getSliderVal(localText, 1)
                        colorSlideB.value = _getSliderVal(localText, 2)
                        widthSlide.value = localTHeight ; widthSlide.maxVal = 128
                        chooseText.item.border.color = theme.palette.normal.selection
                        break;
                    }
                    case 3: {
                        colorSlideR.value = _getSliderVal(localDatum, 0)
                        colorSlideG.value = _getSliderVal(localDatum, 1)
                        colorSlideB.value = _getSliderVal(localDatum, 2)
                        widthSlide.value = localDWidth ; widthSlide.maxVal = 64
                        chooseDatum.item.border.color = theme.palette.normal.selection
                        break;
                    }
                    case 4: {
                        colorSlideR.value = _getSliderVal(localGrid, 0)
                        colorSlideG.value = _getSliderVal(localGrid, 1)
                        colorSlideB.value = _getSliderVal(localGrid, 2)
                        widthSlide.value = localGWidth ; widthSlide.maxVal = 64
                        chooseGrid.item.border.color = theme.palette.normal.selection
                        break;
                    }
                }
                oldId = chosenId
            } else {
                switch (chosenId) {
                    case 1: {
                        localBackground = swatch.color
                        break;
                    }
                    case 2: {
                        localText = swatch.color
                        localTHeight = Math.round(widthSlide.value)
                        DemoLvl.textHt = localTHeight
                        break;
                    }
                    case 3: {
                        localDatum = swatch.color
                        localDWidth = Math.round(widthSlide.value)
                        DemoLvl.levelWidth = localDWidth
                        break;
                    }
                    case 4: {
                        localGrid = swatch.color
                        localGWidth = Math.round(widthSlide.value)
                        DemoLvl.levelWidth = localGWidth
                        break;
                    }
                }
                oldId = 0
            }
        }
    }

    function setSwatch() {
        if ( swatch != null ) { swatch.color = _makeColorString() }
    }

    Component.onDestruction: {
        _backgndColor = localBackground
        _textColor = localText
        _textHeight = localTHeight
        _datumColor = localDatum
        _datumWidth = localDWidth
        _gridColor = localGrid
        _gridWidth = localGWidth
    }

    ColumnLayout {
        id: contentColumn
        width: parent.width / 2
        anchors {
            left: parent.left;
            top: header.bottom;
            margins: units.gu(2)
        }
        RowLayout {
            height: root.height / 2
            spacing: units.gu(5)

            ColumnLayout {
                id: colorSelection
                spacing: units.gu(.5)
                width: root.width / 2 - units.gu(6)
                anchors.rightMargin: 0

                Button {
                    id: rpButton
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                    width: localButtonWidth
                    text: i18n.tr("Restore Previous")
                    color: theme.palette.normal.negative
                    onClicked: fixColors(false)
                }
                Button {
                    id: stButton
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                    width: localButtonWidth
                    text: i18n.tr("Set to Theme")
                    color: theme.palette.normal.positive
                    onClicked: fixColors(true)
                }
                Rectangle {//spacer
                    height: selectorHeight *3/4 ; width: localButtonWidth;
                    color: theme.palette.normal.background
                }

                Component {
                    id: colorSelButton
                    Rectangle {
                        height: selectorHeight ; width: localButtonWidth; radius: localButtonRadius
                        color: theme.palette.normal.background //localBackground
                        border.color: theme.palette.normal.backgroundTertiaryText
                        border.width: localButtonBorder
                        property real compID: 0
                        property alias buttonText: innerLabel.text
                        property alias buttonTextColor: innerLabel.color
                        Label {
                            id: innerLabel
                            text: ""
                            color: theme.palette.normal.backgroundSecondaryText
                            anchors.centerIn: parent
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: { setChoice(compID) }
                        }
                    }
                }
                Loader {
                    id: chooseBkgnd
                    sourceComponent: colorSelButton
                    onLoaded: {
                        item.compID = 1
                        item.buttonText = i18n.tr("Background")
                    }
                }
                Loader {
                    id: chooseText
                    sourceComponent: colorSelButton
                    onLoaded: {
                        item.compID = 2
                        item.buttonText = i18n.tr("Text")
                    }
                }
                Loader {
                    id: chooseDatum
                    sourceComponent: colorSelButton
                    onLoaded: {
                        item.compID = 3
                        item.buttonText = i18n.tr("Datum")
                    }
                }
                Loader {
                    id: chooseGrid
                    sourceComponent: colorSelButton
                    onLoaded: {
                        item.compID = 4
                        item.buttonText = i18n.tr("Grid")
                    }
                }
                Rectangle {//spacer
                    height: selectorHeight / 2 ; width: localButtonWidth;
                    color: theme.palette.normal.background
                }
                Rectangle {
                    id: swatch
                    height: selectorHeight ; width: localButtonWidth; radius: localButtonRadius
                    color: theme.palette.normal.backgroundTertiaryText
                }

            }

            DemoLvl {
                id: replicaLevel
                height: root.height/2
                width: root.width/ 2
                radius: localButtonRadius
                color: localBackground
                textColor: localText
                gridColor: localGrid
                datumColor: localDatum
                levelWidth: localDWidth
                gridWidth: localGWidth
            }
        }

        Column {
            spacing: units.gu(.5)
            MultiUseSlide {
                id: colorSlideR
                caption: i18n.tr('R')
                boxColor: UbuntuColors.red
                value: 128
            }
            MultiUseSlide {
                id: colorSlideG
                caption: i18n.tr('G')
                boxColor: UbuntuColors.green
                value: 128
            }
            MultiUseSlide {
                id: colorSlideB
                caption: i18n.tr('B')
                boxColor: UbuntuColors.blue
                value: 128
            }
            Rectangle {//spacer
                height: selectorHeight *3/4 ; width: localButtonWidth;
                color: theme.palette.normal.background
            }
            MultiUseSlide {
                id: widthSlide
                caption: i18n.tr('W')
                boxColor: theme.palette.normal.backgroundTertiaryText
                value: 64
                maxVal: 128
                stepSize: 4.0
            }

            Button { // vertical spacer
                id: accelButton
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                width: localButtonWidth
                text: i18n.tr("Accelerometers")
                color: 'transparent'
                //onTriggered: {mainStack.pop() ; mainStack.push(Qt.resolvedUrl('AccelPage.qml'))}
            }
            RowLayout {
                spacing: units.gu(5)

                Button {
                    id: compButton
                    property bool caseCompensation: false
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                    width: localButtonWidth
                    text: i18n.tr("Compensation")
                    color: (caseCompensation) ? theme.palette.normal.positive :'transparent' 
//(caseCompensation) ? theme.palette.normal.positive : theme.palette.normal.negative
                    onClicked: caseCompensation = !caseCompensation
                }

                Button {
                    id: calibButton
                    //visible: false
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                    width: localButtonWidth
                    text: i18n.tr("__________Calibrate_______________")
                    color: 'transparent' //UbuntuColors.orange
                    onClicked: {mainStack.pop() ; mainStack.push(Qt.resolvedUrl('CalibrationPage.qml'))}
                }
            }
        }
    }
}