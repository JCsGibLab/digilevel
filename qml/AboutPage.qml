import QtQuick 2.9
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import "Components"

Page {
    header: PageTop {pageTitleId: 4}

    Item {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        //contentHeight: contentColumn.height + units.gu(4)

        ColumnLayout {
            id: contentColumn
            anchors {
                left: parent.left;
                top: parent.top;
                right: parent.right;
                margins: units.gu(2)
            }
            spacing: units.gu(2)

            Label {
                Layout.fillWidth: true

                text: i18n.tr('Digital Level v' + Qt.application.version)
                textSize: Label.XLarge
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Item {
                Layout.preferredHeight: logo.height
                Layout.fillWidth: true

                Image {
                    id: logo
                    anchors.centerIn: parent
                    source: '../img/icon.svg'
                }
            }

            Label {
                Layout.fillWidth: true
                text: i18n.tr('A revision of the Level Finder app by the UBPorts team, recreated by JassMan23');
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Label {
                Layout.fillWidth: true
                text: i18n.tr('Includes handcrafted icons by JassMan23');
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Label {
                Layout.fillWidth: true
                text: i18n.tr('Please consider donating if you like it, and want to see more wonderful apps!')
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Label {
                Layout.fillWidth: true
                text: i18n.tr('A big thank-you to the translation team for UBPorts,\n'+  
                'including anne017, advocatux and giemme1969')
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Button {
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                text: i18n.tr('Donate')
                color: UbuntuColors.orange
                onClicked: Qt.openUrlExternally('https://ubports.com/donate')
            }
        }
    }
}