import QtQuick 2.9
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import "Components"

Page {
    header: PageTop { pageTitleId: 1 }

    property real localButtonRadius: units.gu(1.2)
    property real localButtonBorder: units.gu(0.2)

    function clearText() {
        valuesField.text = ""
    }

    function fancyTheta() {
        if(abslvl){
            if (levelOrientation == 0) {
                return Number(lastTheta).toLocaleString(Qt.locale(),'f',2) + '°' + i18n.tr('H')
            } else {
                return Number(lastTheta-levelOrientation).toLocaleString(Qt.locale(),'f',2) + '°' + i18n.tr('V')
            }
        } else {
            if (levelOrientation == 0) {
                return Number(lastTheta).toLocaleString(Qt.locale(),'f',2) + '°' + i18n.tr('H') + 
                ",rel to " +  Number(relTheta).toLocaleString(Qt.locale(),'f',2) + "°"
            } else {
                return Number(lastTheta-levelOrientation).toLocaleString(Qt.locale(),'f',2) + '°' + i18n.tr('V') + 
                ",rel to " +    Number(relTheta).toLocaleString(Qt.locale(),'f',2) + "°"
            }
        }
    }

    ColumnLayout{
        spacing: units.gu(0.5)
        anchors {
            top: header.bottom
            bottom: undefined
            margins: units.gu(1)
        }
        Rectangle {
            id: heldValues
            Layout.alignment: Qt.AlignTop
            Layout.preferredWidth: root.width
            Layout.minimumHeight: units.gu(7)
            implicitHeight: _F1.implicitHeight + _F2.implicitHeight  + descrField.implicitHeight
            Column {
                anchors.centerIn: parent
                spacing: units.gu(0.5)
                Label {
                    id: _F1
                    Layout.fillWidth: true
                    text: i18n.tr('Held value: ') + fancyTheta()
                    horizontalAlignment: Text.AlignHCenter
                }
                Label {
                    id: _F2
                    Layout.fillWidth: true
                    text: Qt.formatDateTime(new Date(), "dddd @ hh:mm:ss")
                    horizontalAlignment: Text.AlignHCenter
                }
                TextField {
                    id: descrField
                    inputMethodHints: Qt.ImhNoPredictiveText
                    placeholderText: i18n.tr('Description')
                    text: ""
                    onAccepted: {
                        if ((_list.count>0) && (_F2.text == i18n.tr('DateTime: ') + _list.currentItem.myData.dateTime)) {
                            _list.currentItem.myData.description = descrField.text
                        }
                        else if (text.length >0) {
                            _notesList.append({
                                "angle": fancyTheta(), 
                                "dateTime": Qt.formatDateTime(new Date(), "dddd @ hh:mm:ss"),
                                "description": descrField.text
                            })
                        } else {
                            _notesList.append({
                                "angle": fancyTheta(), 
                                "dateTime": Qt.formatDateTime(new Date(), "dddd @ hh:mm:ss"),
                                "description": i18n.tr('New Reading')
                            })
                        }
                        _F1.text = ""
                        _F2.text = ""
                        descrField.visible = false
                    }
                }
            }
        }


        Rectangle {
            Layout.alignment: Qt.AlignVCenter
            Layout.preferredWidth: root.width - anchors.margins * 2
            Layout.preferredHeight: root.height * 2 / 3
            Layout.fillHeight: true
            Rectangle {
                id: _listBox
                anchors.fill: parent
                anchors.margins: units.gu(1.2)
                radius: localButtonRadius
                border.color: theme.palette.normal.backgroundTertiaryText
                border.width: localButtonBorder
                Rectangle {
                    radius: localButtonRadius
                    anchors.fill: parent
                    anchors.margins: units.gu(1.2)
                    Layout.fillHeight: true

                    ListView {
                        id: _list
                        clip: true

                        anchors.fill: parent
                        model: _notesList
                        delegate: Item {
                            width: parent.width; height: 160
                            property variant myData: model
                            Column {
                                Text { text: '<b>' + i18n.tr('Angle:') + '</b> ' + angle }
                                Text { text: '<b>' + i18n.tr('DateTime:') + '</b> ' + dateTime }
                                Text { text: '<b>' + i18n.tr('Description:') + '</b> ' + description }                         
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    _list.currentIndex = index
                                }
                            }   
                        }

                        highlight: Rectangle { color: "lightsteelblue"; radius: localButtonRadius }
                        focus: true
                        property bool previouslyRun: false
                        onCurrentIndexChanged: {
                            if (previouslyRun) {
                                _F1.text = i18n.tr('Angle: ') + _list.currentItem.myData.angle
                                _F2.text = i18n.tr('DateTime: ') + _list.currentItem.myData.dateTime
                                descrField.text = _list.currentItem.myData.description
                                descrField.visible = true
                            } else {previouslyRun = true}
                        }
                    }
                }
            }
        }

        Rectangle {
            Layout.alignment: Qt.AlignBottom
            Layout.preferredWidth: root.width
            Layout.minimumHeight: units.gu(7)
            implicitHeight: Math.max(_B1.implicitHeight, _B2.implicitHeight)
            
            Row {
                anchors.centerIn: parent
                spacing: units.gu(2)
                Button {
                    id: _B1
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                    text: i18n.tr('Clear Current')
                    color: UbuntuColors.orange
                    onClicked: _notesList.remove(_list.currentIndex)
                }
                Button {
                    id: _B2
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                    text: i18n.tr('Clear All')
                    color: UbuntuColors.red
                    onClicked: _notesList.clear()
                }
            }
        }
    }
}
